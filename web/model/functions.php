<?php
    function conectarBanco($host, $user, $pass, $base){
        return mysqli_connect($host, $user, $pass, $base);
    }

    function consultar($conection, $query, $useResult){
        if($useResult){
            return $conection->query($query, MYSQLI_USE_RESULT);
        }else{
            return $conection->query($query);
        }
    }

    function verificaUsuario($result, $email, $senha){
        $data = array();
        while($row = mysqli_fetch_assoc($result)){
            array_push($data, $row);
        }
        if(sizeof($data) > 0){
            // echo $data[0]["email"]." ".$data[0]["senha"];
            if($data[0]["senha"] == $senha){
                return true;
            }else{
                return 101;
            }
        }else{
            return 100;
        }
    }

?>
