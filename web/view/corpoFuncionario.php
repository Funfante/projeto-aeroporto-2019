<?php  include "topoCorpo.html";?>
<link rel="stylesheet" href="css/menu.css">
<style>
  .cabecalho nav ul{
    width: calc(15vw * 2);
  }
  .menu.active{
    height: 30vh;
  }
</style>
  <li class="menu" id="menu">

    <img src="svg/menu.svg" class="btn">
    <img src="svg/delete.svg" class="titulo" id="titulo">
    <!-- <span class="titulo" id="titulo">Meus dados</span> -->

    <div class="botoes">
      <a href="perfil.php" class="opcao">
        <img src="svg/user.svg" alt="" class="icone">
        <span class="texto">Meu Perfil</span>
      </a>

      <a class="opcao" id='logout'>
        <img src="svg/logout.svg" alt="" class="icone">
        <span class="texto">Logout</span>
      </a>
    </div>

  </li>
<?php  include "baseCorpo.html";?>
