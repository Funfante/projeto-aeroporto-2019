<?php  include "topoCorpo.html";?>
<link rel="stylesheet" href="css/menu.css">
<style>
  .cabecalho nav ul{
    width: calc(15vw * 5);
  }
  .menu.active{
    height: 40vh;
  }
</style>
  <li class="atalho">
    <a href="#viajar">
      <img src="svg/viajar.svg" alt="" class="icone">
      <span class="texto">Voar</span>
    </a>
  </li>

  <li class="atalho">
    <a href="#decolagens">
      <img src="svg/decolagens.svg" alt="" class="icone">
      <span class="texto">Decolagens</span>
    </a>
  </li>

  <li class="atalho">
    <a href="#aterrisagens">
      <img src="svg/aterrisagens.svg" alt="" class="icone">
      <span class="texto">Aterrisagens</span>
    </a>
  </li>

  <li class="menu" id="menu">

    <img src="svg/menu.svg" class="btn">
    <img src="svg/delete.svg" class="titulo" id="titulo">
    <!-- <span class="titulo" id="titulo">Meus dados</span> -->

    <div class="botoes">
      <a href="perfil.php" class="opcao">
        <img src="svg/user.svg" alt="" class="icone">
        <span class="texto">Meu Perfil</span>
      </a>

      <a href="#b" class="opcao">
        <img src="svg/cart.svg" alt="" class="icone">
        <span class="texto">Minhas passagens</span>
        <!-- <img display: inline-block;src="" alt=""> -->
      </a>

      <a class="opcao" id='logout'>
        <img src="svg/logout.svg" alt="" class="icone">
        <span class="texto">Logout</span>
      </a>
    </div>

  </li>
<?php  include "baseCorpo.html";?>
