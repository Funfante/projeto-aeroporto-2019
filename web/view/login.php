<?
  session_start();
?>
<html lang="pt">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/loginSpans.css">
    <script src="js/sendLogin.js" charset="utf-8"></script>
  </head>
  <body>
    <form id="login" method="post">
      <div class="input um">
        <input type="text" name="cod" id="cod">
        <span class='notify' id="spnCod"></span>
      </div>

      <div class="input dois">
        <input type="password" name="senha" id="senha">
        <span class='notify' id="spnSenha"></span>
      </div>

      <div class="tres">
        <label>Não possui uma conta?</label>
        <a href="cadastro.php">Cadastre-se aqui</a>
      </div>


      <div class="input quatro">
        <input type="submit" value="Entrar" id="submit">
        <span class='notify' id="spnSubmit"></span>
      </div>


    </form>
  </body>
</html>
