<?php include '../controller/sessionBase.php'; ?>
<?php  include 'cabecalho.html';?>
  <title>Meu perfil</title>
  <link rel="stylesheet" href="css/perfil.css">
  <link href="https://fonts.googleapis.com/css?family=Orbitron&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
<?php include 'corpo.html'; ?>
  <main>
    <div class="dados">

      <button class="btn btnEdit btnActive" id="editData">
        <img src="svg/edit.svg">
      </button>

      <button class="btn btnSave" id="saveData">
        Salvar
      </button>

      <button class="btn btnDiscard" id="discardData">
        Cancelar
      </button>

        <form id="dadosUsuario">
          <div class="usuario">
            <div class="imgContainer">
              <img id="profilePic" src="#"/>
              <input type="file" name="profilePic" id="setProfilePic" alt="" disabled>
            </div>

            <input type="text" name="nome" value="Fulano de tals e çksjfhdaçlshfçsahgaçhga~ishga~psjdáojksdaksdc~skadóiqwa~dkaosdsal,dawojdçslamdaowjdmsalçmjdas" class="nome" disabled>
            <input type="text" name="endereco" value="R. Qualquer, 91, Jd.São Vicente, Osasco, SP" class="endereco" disabled>

            <div class="informacoes">
              <input type="text" name="" value="" disabled>
              <input type="text" name="" value="" disabled>
              <input type="text" name="" value="" disabled>
              <input type="text" name="" value="" disabled>
            </div>

          </div>
        </form>

      </div>

    <div class="voos">

      <h3>Meus voos</h3>
      <div class="meusVoos">
        <a href="#idVoo" class="voo">
          <p class="nomeVoo">Cancún</p>
          <p class="dataPartida">09/12</p>
          <p class="dataChegada">21/01</p>
          <!--Bolinha colorida pra representar o status do voo-->
          <span class="status"></span>
        </a>

        <a href="#idVoo" class="voo">
          <p class="nomeVoo">nome</p>
          <p class="dataPartida">22/01</p>
          <p class="dataChegada">11/09</p>
          <!--Bolinha colorida pra representar o status do voo-->
          <span class="status"></span>
        </a>

        <a href="#idVoo" class="voo">
          <p class="nomeVoo">nome</p>
          <p class="dataPartida">horaP</p>
          <p class="dataChegada">horaC</p>
          <!--Bolinha colorida pra representar o status do voo-->
          <span class="status"></span>
        </a>

        <a href="#idVoo" class="voo">
          <p class="nomeVoo">nome</p>
          <p class="dataPartida">horaP</p>
          <p class="dataChegada">horaC</p>
          <!--Bolinha colorida pra representar o status do voo-->
          <span class="status"></span>
        </a>

        <a href="#idVoo" class="voo">
          <p class="nomeVoo">nome</p>
          <p class="dataPartida">horaP</p>
          <p class="dataChegada">horaC</p>
          <!--Bolinha colorida pra representar o status do voo-->
          <span class="status"></span>
        </a>
      </div>

    </div>

    <div class="cartoes">
      <h3>Meus cartoes</h3>
      <button id="btn editarCartoes"></button>
      <div class="meusCartoes">

        <div class="cartao">
          <img src="#" class="miniatura">
          <h4 class="num">WWWW-WWW-WW</h4>
          <h5 class="detalhes">dados</h5>
        </div>

        <div class="cartao">
          <img src="#" class="miniatura">
          <h4 class="num">XXXX-XXX-XX</h4>
          <h5 class="detalhes">dados</h5>
        </div>

        <div class="cartao">
          <img src="#" class="miniatura">
          <h4 class="num">YYYY-YYY-YY</h4>
          <h5 class="detalhes">dados</h5>
        </div>

        <div class="cartao">
          <img src="#" class="miniatura">
          <h4 class="num">ZZZZ-ZZZ-ZZ</h4>
          <h5 class="detalhes">dados</h5>
        </div>

      </div>
    </div>
  </main>

    <button class="notificacoes btn" id="notificacoes">
      <img src="svg/bell.svg"/>
      <span></span>
    </button>
<script src="js/perfil.js" charset="utf-8"></script>
<?php include 'rodape.html'; ?>
