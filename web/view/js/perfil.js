window.onload = function(){
  console.log("teste");
  var notificacoes = document.getElementById('notificacoes');
  var editDados = document.getElementById('editData');
  var saveDados = document.getElementById('saveData');
  var discardDados = document.getElementById('discardData');
  var inputs = document.getElementsByTagName("input");
  var form = document.getElementById("dadosUsuario");

  editDados.addEventListener("click", function(){
      editDados.setAttribute("class", "btn btnEdit");
      saveDados.setAttribute("class", "btn btnSave btnActive");
      discardDados.setAttribute("class", "btn btnDiscard btnActive");
      for(let i = 0; i < inputs.length; i++){
        inputs[i].disabled = false;
      }
  });

  saveDados.addEventListener("click", function(){
    discardDados.setAttribute("class", "btn btnDiscard");
    saveDados.setAttribute("class", "btn btnSave");
    editDados.setAttribute("class", "btn btnEdit btnActive");
    for(let i = 0; i < inputs.length; i++){
      inputs[i].disabled = true;
    }
    var formData = new FormData(form);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "../controller/atualizarDados.php", true);
  });

  discardDados.addEventListener("click", function(){
    saveDados.setAttribute("class", "btn btnSave");
    discardDados.setAttribute("class", "btn btnDiscard");
    editDados.setAttribute("class", "btn btnEdit btnActive");
    for(let i = 0; i < inputs.length; i++){
      inputs[i].disabled = true;
    }
  });
}
