window.onload = function (){
  var xhr = new XMLHttpRequest();

  var formulario = document.getElementById('login');
  xhr.open("POST", "../../controller/isLogged.php", true);
  xhr.send();
  xhr.responseType = "json";
  xhr.onreadystatechange = function(){
    if(xhr.readyState == 0){}

    if(xhr.readyState == 1){}

    if(xhr.readyState == 2){}

    if(xhr.readyState == 3){}

    if(xhr.readyState == 4){
      var resposta = xhr.response;
      if(resposta){
        window.location.href = "index.php?msg=logged";
      }
    }

  }


  formulario.onsubmit = function(e){
    e.preventDefault();
    var data = new FormData(formulario);
    // var link = ?email="+email+"&senha="+senha;
    // console.log(data);
    xhr.open("POST", "../../controller/verificaLogin.php", true);
    xhr.send(data);
    // xhr.responseType = "text";
    xhr.onreadystatechange = function(){
      var submit = document.getElementById('spnSubmit');
      var cod = document.getElementById('spnCod');
      var senha = document.getElementById('spnSenha');

      if(xhr.readyState == 0){}

      if(xhr.readyState == 1){}

      if(xhr.readyState == 2){}

      if(xhr.readyState == 3){
        submit.setAttribute('class', 'notify loading');
        senha.setAttribute('class', 'notify');
        cod.setAttribute('class', 'notify');

      }

      if(xhr.readyState == 4){
        submit.setAttribute('class', 'notify');

        let resposta = xhr.response;
        // let resposta = xhr.responseText;
        console.log(resposta);
        if(resposta.isLogged){
          window.location.href = "index.php";
        }

        //Posiveis erros
        //  98: campo de email vazio
        //  99: campo de senha vazio
        // 100: email incorreto
        // 101: senha incorreta
        // 200: Impossivel conectar com a base de dados
        // 201: Incapaz de realizar consultar

        if(resposta.error[0] == 98){
          cod.setAttribute('class', 'notify empty');
          cod.innerHTML = "Digite seu codigo!";
        }else if(resposta.error[0] == 99){
          senha.setAttribute('class', 'notify empty');
          senha.innerHTML = "Digite sua senha!";
        }else if(resposta.error[0] == 100){
          cod.setAttribute('class', 'notify notFound');
          cod.innerHTML = "Codigo não cadastrado!";
        }else if(resposta.error[0] == 101){
          senha.setAttribute('class', 'notify doesNotMatch');
          senha.innerHTML = "Senha não confere!";
        }else if(resposta.error[0] == 102){
          cod.setAttribute('class', 'notify notFound');
          cod.innerHTML = "Prefixo de código deve ser um caracter alfabético!";
        }else if(resposta.error[0] == 103){
          cod.setAttribute('class', 'notify notFound');
          cod.innerHTML = "Prefixo de código inválido!";
        }else if(resposta.error[0] == 200 || resposta.error[0] == 201){
          window.location.href = "dataBaseConnectFailure.php?error=" + resposta.error[0];
        }

      }
    }
  }
}
