drop database if exists trabalhoInterdisciplinar;
create database trabalhoInterdisciplinar;
use trabalhoInterdisciplinar;

create table if not exists pessoa(
	telefone varchar(19) not null,
    email varchar(100) not null,
    endereco varchar(150) not null,
    nome varchar(100) not null,
    senha varchar(25) not null,
    codChar char not null,
    cod int not null,
    primary key(codChar, cod)
)engine = innodb;

create table if not exists pessoaFisica(
	rg varchar (11) not null,
    genero char not null,
    dataNascimento date not null,
    cod int not null,
    codChar char not null,
    primary key(codChar, cod),
    foreign key (codChar, cod) references pessoa(codChar, cod)
)engine = innodb;

create table if not exists pessoaJuridica(
	cnpj varchar(11) not null,
    cod int not null primary key auto_increment,
    codChar char default 'J',
    foreign key (codChar, cod) references pessoa(codChar, cod)
)engine = innodb;

create table if not exists Passageiro(
    #cod int not null primary key auto_increment,
    #codChar char default 'P',
    cod int not null primary key auto_increment,
    codChar char not null default 'P',
    foreign key (codChar, cod) references pessoaFisica(codChar, cod)
)engine = innodb;

create table if not exists Empregado(
    carteiraDeTrabalho varchar(25) not null,
    salario float not null,
    cargaHoraria int not null,
    funcao varchar(40) not null,
    setor varchar(40) not null,
    cod int not null,
    codChar char not null,
    primary key(codChar, cod),
    foreign key (codChar, cod) references pessoaFisica(codChar, cod)
)engine = innodb;

create table if not exists Funcionario(
	cod int not null primary key auto_increment,
    codChar char default 'F',
    foreign key (codChar, cod) references pessoaFisica(codChar, cod)
)engine = innodb;

create table if not exists Gerente(
	cod int not null primary key auto_increment,
    codChar char default 'G',
    foreign key (codChar, cod) references pessoaFisica(codChar, cod)
)engine = innodb;

create table if not exists Administrador(
	cod int not null primary key auto_increment,    
	codChar char default 'A',
    foreign key (codChar, cod) references pessoaFisica(codChar, cod)
)engine = innodb;



insert into pessoa(telefone, email, endereco, nome, senha, codChar, cod) values
	('999999999', 'testando@gmail.com', 'r. dos boi taca', 'Joaquim', 'abcde', 'P', 1),
    ('999999998', 'testado@gmail.com', 'r. dos boi taca', 'Adeilton', 'abcde', 'P', 2),
    ('999795969', 'testou@gmail.com', 'r. dos boi taca', 'Mariana', '12345', 'P', 3),
    ('123456789', 'testaria@gmail.com', 'r. dos boi taca', 'Ana', '12345', 'P', 4),
    ('369258147', 'teste@gmail.com', 'r. dos boi taca', 'Nicole', '12345', 'F', 5);
    
insert into pessoaFisica(rg, genero, dataNascimento, cod, codChar) values
	('123456789', 'M', '2018-07-25', 1, 'P'),
    ('234567899', 'M', '2003-04-15', 2, 'P'),
    ('345678999', 'F', '2004-06-19', 3, 'P'),
    ('456789999', 'F', '2004-01-01', 4, 'P'),
    ('567899999', 'F', '2005-06-07', 5, 'F');

insert into Passageiro(cod) values
	(1),
    (2),
    (3),
    (4);

insert into empregado (cod, codChar, carteiraDeTrabalho, salario, funcao, setor, cargaHoraria) value (5, 'F', '125654987', 4500.00, 'Recepcionista', 'Portaria', 8);
insert into Funcionario(cod) value (5);

select * from pessoa;









