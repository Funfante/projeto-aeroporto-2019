package model;
 import java.sql.Date;
// Classe para representar um Empregado:
public class Empregado extends PessoaFisica{
	private String carteiraDeTrabalho; // Numero da carteira de trabalho do empregado.
	private double salario; // Salario do empregado.
	private int cargaHoraria;  // Carga horaria de trabalho semanal do empregado.
	private String funcao; // Funcao do empregado na empresa.
	private String setor; // Setor que o empregado trabalha na empresa.
	private String status;//ferias ou disponivel

	//Construtor da classe Empregado (cria um objeto empregado):
	public Empregado(String email, String telefone, String endereco, String nome, String rg, String genero, Date dataDeNascimento, String carteiraDeTrabalho, double salario, int cargaHoraria, String funcao, String setor, String senha, String cod, String status, String foto) {
		super(telefone,email, endereco, nome, rg, genero, dataDeNascimento, cod,senha, foto);
		this.carteiraDeTrabalho = carteiraDeTrabalho;
		this.salario = salario;
		this.cargaHoraria = cargaHoraria;
		this.funcao = funcao;
		this.setor = setor;
		this.status = status;
	}

	// Construtor vazio de teste:
	public Empregado() {
		super();
	}

	public String getCarteiraDeTrabalho() {
		return carteiraDeTrabalho;
	}

	public void setCarteiraDeTrabalho(String carteiraDeTrabalho) {
		this.carteiraDeTrabalho = carteiraDeTrabalho;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setContato(String email, String telefone, String endereco) {
		setEmail(email);
		setTelefone(telefone);
		setEndereco(endereco);
	}

	// Metodo para exibir empregado:
	public void exibirEmpregado() {
		super.exibirPessoaFisica();
		System.out.println("Carteira de trabalho: " + getCarteiraDeTrabalho());
		System.out.println("Sal�rio: " + getSalario());
		System.out.println("Carga hor�ria: " + getCargaHoraria());
		System.out.println("Fun��o: " + getFuncao());
		System.out.println("Setor: " + getSetor());
	}
}
