package model;

import javax.swing.JOptionPane;

import java.sql.Date;
import java.util.ArrayList;
import model.*;

public class Aeroporto{
	//atributos da classe
	private Agenda agenda;
	private Rh rh;
	private ArrayList<Aeronave> aeronaves;
	private ArrayList<Voo> voos;
	private ArrayList<VooDisponibilizado> voosDisponibilizados;
	private ArrayList<Empregado> administradores;
	private ArrayList<Empresa> empresas;
	private ArrayList<Empregado> pilotos;
	private ArrayList<Empregado> funcionarios;
	private ArrayList<Empregado> gerentes;
	
	//contrutor da classe
	public Aeroporto() {
		this.agenda = new Agenda();
		this.rh = new Rh();
		this.aeronaves = new ArrayList();
		this.voos = new ArrayList();
		this.administradores = new ArrayList();
		this.empresas = new ArrayList();
		this.pilotos = new ArrayList();
		this.funcionarios = new ArrayList();
		this.gerentes = new ArrayList();
		this.voosDisponibilizados = new ArrayList();
	}
	
	//setters e getters
	public Agenda getAgenda() {
		return agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}

	public Rh getRh() {
		return rh;
	}

	public void setRh(Rh rh) {
		this.rh = rh;
	}

	public ArrayList<Aeronave> getAeronaves() {
		return aeronaves;
	}

	public void setAeronaves(ArrayList<Aeronave> aeronaves) {
		this.aeronaves = aeronaves;
	}

	public ArrayList<Voo> getVoos() {
		return voos;
	}


	public void setVoos(ArrayList<Voo> voos) {
		this.voos = voos;
	}


	public ArrayList<Empregado> getAdministradores() {
		return administradores;
	}

	public void setAdministradores(ArrayList<Empregado> administradores) {
		this.administradores = administradores;
	}

	public ArrayList<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(ArrayList<Empresa> empresas) {
		this.empresas = empresas;
	}

	public ArrayList<Empregado> getPilotos() {
		return pilotos;
	}

	public void setPilotos(ArrayList<Empregado> pilotos) {
		this.pilotos = pilotos;
	}

	public ArrayList<Empregado> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(ArrayList<Empregado> funcionarios) {
		this.funcionarios = funcionarios;
	}

	public ArrayList<Empregado> getGerentes() {
		return gerentes;
	}

	public void setGerentes(ArrayList<Empregado> gerentes) {
		this.gerentes = gerentes;
	}

}
