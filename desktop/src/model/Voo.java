package model;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

// Classe para representar um voo:
public class Voo {

////////// ADICIONAR PASSAGEIROS	
//////////REMOVER PASSAGEIROS	

	private ArrayList<Passageiro> passageiros; // Pessoas que ser�o transportadas na aeronave.
												// //!!!!!!!!!!!!!!!!!!!!
	private Evento partida; 
	private Evento chegada;
	private String autorizado;
	private String tipoCarga; // O que ser� transportado na aeronave como carga (Qual o tipo de carga?).
	private String tipoCP; // O que ser� transportado na aeronave (*carga ou pessoas*).
	private String codigoDeVoo; // Codigo do voo.
	private String senhaVoo;
	private String statusVoo; // Status do voo (decolando, voando, aterrissando, etc).
	private String descricao; // Descri��o do voo.
	private Aeronave aeronave; // Objeto aeronave.
	private String destino; // Destino do voo.
	private String origem; // Origem do voo.
	private String piloto; // Qual piloto ir� pilotar a aeronave.
	private int qtdPassageiros; // Quantidade maxima de passageiros.
	private int capacidadeMax;

	public Voo() {
		super();
    }
	

	public Voo(Evento partida, Evento chegada, String autorizado, String tipoCarga, String tipoCP,
			String codigoDeVoo, String senhaVoo, String statusVoo, String descricao, Aeronave aeronave, String destino, String origem,
			String piloto, int qtdPassageiros) {
		super();
		this.partida = partida;
		this.chegada = chegada;
		this.autorizado = autorizado;
		this.tipoCarga = tipoCarga;
		this.tipoCP = tipoCP;
		this.codigoDeVoo = codigoDeVoo;
		this.senhaVoo = senhaVoo;
		this.statusVoo = statusVoo;
		this.descricao = descricao;
		this.aeronave = aeronave;
		this.destino = destino;
		this.origem = origem;
		this.piloto = piloto;
		this.qtdPassageiros = qtdPassageiros;
		passageiros = new ArrayList(this.qtdPassageiros);
	}

	// Getters e Setters:
	public String getCodigoDeVoo() {
		return codigoDeVoo;
	}

	public void setCodigoDeVoo(String codigoDeVoo) {
		this.codigoDeVoo = codigoDeVoo;
	}

	public String getSenhaVoo() {
		return senhaVoo;
	}


	public void setSenhaVoo(String senhaVoo) {
		this.senhaVoo = senhaVoo;
	}


	public String getStatusVoo() {
		return statusVoo;
	}

	public void setStatusVoo(String statusVoo) {
		this.statusVoo = statusVoo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Evento getPartida() {
		return partida;
	}

	public void setPartida(Evento partida) {
		partida.setTipo(1);
		this.partida = partida;
	}

	public Evento getChegada() {
		return chegada;
	}

	public void setChegada(Evento chegada) {
		chegada.setTipo(1);
		this.chegada = chegada;
	}

	public String getAutorizado() {
		return autorizado;
	}


	public void setAutorizado(String autorizado) {
		this.autorizado = autorizado;
	}


	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getPiloto() {
		return piloto;
	}

	public void setPiloto(String piloto) {
		this.piloto = piloto;
	}

	public String getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public String getTipoCP() {
		return tipoCP;
	}

	public void setTipoCP(String tipoCP) {
		this.tipoCP = tipoCP;
	}

	public Aeronave getAeronave() {
		return aeronave;
	}

	public void setAeronave(Aeronave aeronave) {
		this.aeronave = aeronave;
	}

	public ArrayList<Passageiro> getPassageiros() {
		return passageiros;
	}

	public void setPassageiros(ArrayList<Passageiro> passageiros) {
		this.passageiros = passageiros;
	}

	public int getQuantidadePassageiros() {
		return passageiros.size();
	}

	public int getQtdPassageiros() {
		return qtdPassageiros;
	}

	public void setQtdPassageiros(int qtdPassageiros) {
		this.qtdPassageiros = qtdPassageiros;
	}

	public int getCapacidadeMax() {
		return capacidadeMax;
	}


	public void setCapacidadeMax(int capacidadeMax) {
		this.capacidadeMax = capacidadeMax;
	}


	// Metodo para imprimir passageiros:
	public void imprimirPassageiros() {
		for (int i = 0; i < getQuantidadePassageiros(); i++) {
			passageiros.get(i).exibirPessoa();
		}
	}

	// Metodo para exibir voo:
	public void exibirVoo() {
		System.out.println("Codigo de voo: " + getCodigoDeVoo() + " .");
		System.out.println("Status do voo: " + getStatusVoo() + ".");
		System.out.println("Descricao do voo: " + getDescricao() + ".");
		System.out.println("Horario de partida: " + partida.getHora() + ".");
		System.out.println("Horario de partida: " + chegada.getHora() + ".");
		System.out.println("Data de partida: " + partida.getData() + ".");
		System.out.println("Data de chegada: " + chegada.getData() + ".");
		System.out.println("Destino do voo: " + getDestino() + ".");
		System.out.println("Origem do voo:" + getOrigem() + ".");
		System.out.println("Piloto: " + getPiloto() + ".");
		System.out.println("Na aeronave teremos o transporte de : " + getTipoCP() + ".");// Pessoas ou carga.
		if (getTipoCP() == "Carga") {
			System.out.println("Tipo de carga que ser� transportada: " + getTipoCarga());
		}
		if (getTipoCP() == "Pessoas") {
			imprimirPassageiros();
		}
	}
	
	//Metodo para consultar voo:
	public void consultarVoo() {
		System.out.println("Data do voo: "+ partida.getData());
		System.out.println("Horario do voo: "+ partida.getHora());
		System.out.println("C�digo do voo: "+ getCodigoDeVoo());
		System.out.println("Capacidade m�xima de passageiros no voo: "+ aeronave.getCapacidadeMaxima());
	}
	
}