package model;

public class Pessoa {
	private String telefone;
	private String email;
	private String endereco;
	private String cod;
	private String senha;
	private String foto;

	
	public Pessoa(String telefone, String email, String endereco, String cod, String senha, String foto) {
		super();
		this.telefone = telefone;
		this.email = email;
		this.endereco = endereco;
		this.cod = cod;
		this.senha = senha;
	}
	
	public Pessoa() {
		
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public String getCod(){
		return cod;
	}
	
	public void setCod(String cod){
		this.cod = cod;
	}
	
	public String getSenha(){
		return senha;
	}
	
	public void setSenha(String senha){
		this.senha = senha;
	}
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public void exibirPessoa() {
		System.out.println("Telefone: " + getTelefone());
		System.out.println("Email: " + getEmail());
		System.out.println("Endere�o: " + getEndereco());
		System.out.println("C�digo: " + getCod());
	}
}
