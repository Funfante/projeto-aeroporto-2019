package model;

import java.util.Date;
import java.util.ArrayList;

public class Agenda {
	//atributos
	private ArrayList<Evento> eventos;
	private Date dia;
	
	//construtor da classe
	public Agenda() {
		eventos = new ArrayList<Evento>();
	}
	
	//getters e setters
	public ArrayList<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(ArrayList<Evento> eventos) {
		this.eventos = eventos;
	}
	
	public Date getDia() {
		return dia;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	public void adicionarVoo(Voo voo) {
		adicionarEvento(voo.getPartida());
		adicionarEvento(voo.getChegada());
	}
	
	public void adicionarEvento(Evento evento) {
		int cont = 0;
		for(int i = 0; i < eventos.size(); i++) {
			if(evento.getCod().equals(eventos.get(i).getCod())) {
				cont++;
			}
		}
		if(cont == 0) {
			eventos.add(evento);
		}else
			if(cont == 1 || cont > 1) {
				System.out.println("Evento j� existe.");
			}
	}
	
	public void exibirAgenda() {
		System.out.println("Dia referente:" + getDia());
		System.out.println("Voos:");
		imprimirEventos(1);
		System.out.println("Eventos:");
		imprimirEventos(0);
		
	}
	
	//metodo para imprimir todos eventos
	public void imprimirEventos() {
		for(Evento evento:eventos) {
			if(evento.getData().compareTo(getDia()) == 0) {
				evento.exibirEvento();
			}				
		}
	}
	
	//metodo para imprimir eventos de um determinado tipo
	public void imprimirEventos(int tipo) {
		for(Evento evento:eventos) {
			if(evento.getTipo() == tipo) {
				if(evento.getData().compareTo(getDia()) == 0) {
					evento.exibirEvento();
				}				
			}
		}
	}
	
	//metodo para imprimir evento especifico
	public void imprimirEvento(String cod) {
		for(Evento evento:eventos) {
			if(evento.getCod().equals(cod)) {
				evento.exibirEvento();
			}				
		}
	}

	
}
