
package model;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Rh {

	private ArrayList<Empregado> empregados;

	public Rh() {

		empregados = new ArrayList<Empregado>();

		this.empregados.add(new Funcionario());

	}

	public ArrayList<Empregado> getEmpregados() {

		return empregados;

	}

	public void setEmpregados(ArrayList<Empregado> empregados) {

		this.empregados = empregados;

	}

	public void contratarEmpregado(Empregado empregado) {

		boolean existe = false;

		for (int cont = 0; cont < empregados.size(); cont++) {

			if (empregados.get(cont).getCod().equals(empregado.getCod())) {

				existe = true;

				break;

			}

		}

		if (!existe) {

			empregados.add(empregado);
			String mensagem = "Empregado contratado com sucesso.";

			String titulo = "Sucesso";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);

		} else {
			String mensagem = "C�digo j� est� sendo usado.";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);

		}

	}

	public void demitirEmpregado(String cod) {

		int cont = 0, i, indice = 0;

		for (i = 0; i < empregados.size(); i++) {

			if (empregados.get(i).getCod().equals(cod)) {

				cont++;

				indice = i;

			}

		}

		if (cont == 0) {

			String mensagem = "N�o existem empregados cadastrados com esse identificador, ou este identificador � inv�lido.";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		} else if (cont == 1) {

			empregados.remove(indice);
			String mensagem = "Empregado demitido com sucesso.";

			String titulo = "Sucesso";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);

		} else if (cont > 1) {

			String mensagem = "Existe mais de um empregado com este c�digo.";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);

		}

	}

	public void exibirEmpregados() {

		for (int cont = 0; cont < empregados.size(); cont++) {

			empregados.get(cont).exibirEmpregado();

		}

	}

	public void exibirEmpregado(String identificador) {

		int num = 0;

		for (int cont = 0; cont < empregados.size(); cont++) {

			String letra = String.valueOf(Character.toUpperCase(empregados.get(cont).getCod().charAt(0)));

			if (letra.equals(identificador)) {

				empregados.get(cont).exibirEmpregado();

				num++;

			}

		}

		if (num == 0) {
			String mensagem = "N�o existem empregados cadastrados com esse identificador, ou este identificador � inv�lido.";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		}

	}

	public void exibirEmpregadosFerias() {
		for (int i = 0; i < empregados.size(); i++) {
			if (empregados.get(i).getStatus().equals("em ferias")) {
				empregados.get(i).exibirEmpregado();
			}
		}
	}

	public void exibirEmpregadosDisponiveis() {
		for (int i = 0; i < empregados.size(); i++) {
			if (empregados.get(i).getStatus().equals("disponivel")) {
				empregados.get(i).exibirEmpregado();
			}
		}
	}

	public void alterarSenhaEmpregado(String cod, String novaSenha) {

		int cont = 0, indice = 0;

		for (int i = 0; i < empregados.size(); i++) {

			if (empregados.get(i).getCod().equals(cod)) {

				cont++;

			}

		}

		if (cont == 0) {

			String mensagem = "Empregado n�o existe no sistema!";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		} else

		if (cont == 1) {

			empregados.get(indice).setSenha(novaSenha);
			String mensagem = "Senha alterada com sucesso.";

			String titulo = "Sucesso";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
		} else

		if (cont > 1) {

			String mensagem = "Existe mais de um empregado com este c�digo.";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);

		}

	}

	public void alterarSalarioEmpregado(String cod, double novoSalario) {

		int indice = 0, cont = 0;

		for (int i = 0; i < empregados.size(); i++) {

			if (empregados.get(i).getCod().equals(cod)) {

				indice = i;

				cont++;

			}

		}

		if (cont == 0) {

			String mensagem = "Empregado n�o existe no sistema!";

			String titulo = "Erro";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		} else

		if (cont == 1) {

			empregados.get(indice).setSalario(novoSalario);
			String mensagem = "Salario alterado com sucesso.";

			String titulo = "Sucesso";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
		} else

		if (cont > 1) {

			String mensagem = "Existe mais de um empregado com este c�digo.";

			String titulo = "Erro";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);

		}

	}
	
	public void alterarCargaHorariaEmpregado(String cod, int novaCargaHoraria) {
		int cont = 0, indice = 0;
		for(int i = 0 ; i < empregados.size(); i++) {
			if(empregados.get(i).getCod().equals(cod)) {
				indice = i;
				cont++;
			}
		}
		
		if(cont == 0) {
			String mensagem = "Empregado n�o existe no sistema!";

			String titulo = "Erro";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		}else
			if(cont == 1) {
				empregados.get(indice).setCargaHoraria(novaCargaHoraria);
				String mensagem = "Carga horaria alterada com sucesso.";

				String titulo = "Sucesso";

				JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
			}else
				if(cont > 1) {
					String mensagem = "Existe mais de um empregado com este c�digo.";

					String titulo = "Erro";

					JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
				}
	}
	
	public void alterarFuncao(String cod, String novaFuncao) {
		int cont = 0, indice = 0;
		for(int i = 0 ; i < empregados.size(); i++) {
			if(empregados.get(i).getCod().equals(cod)) {
				indice = i;
				cont++;
			}
		}
		
		if(cont == 0) {
			String mensagem = "Empregado n�o existe no sistema!";

			String titulo = "Erro";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		}else
			if(cont == 1) {
				empregados.get(indice).setFuncao(novaFuncao);
				String mensagem = "Funcao alterada com sucesso.";

				String titulo = "Sucesso";

				JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
			}else
				if(cont > 1) {
					String mensagem = "Existe mais de um empregado com este c�digo.";

					String titulo = "Erro";

					JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
				}
	}
	
	public void alterarSetor(String cod, String novoSetor) {
		int cont = 0, indice = 0;
		for(int i = 0 ; i < empregados.size(); i++) {
			if(empregados.get(i).getCod().equals(cod)) {
				indice = i;
				cont++;
			}
		}
		
		if(cont == 0) {
			String mensagem = "Empregado n�o existe no sistema!";

			String titulo = "Erro";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		}else
			if(cont == 1) {
				empregados.get(indice).setSetor(novoSetor);
				String mensagem = "Setor alterado com sucesso.";

				String titulo = "Sucesso";

				JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
			}else
				if(cont > 1) {
					String mensagem = "Existe mais de um empregado com este c�digo.";

					String titulo = "Erro";

					JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
				}
	}
	
	public void darFerias(String cod) {
		int cont = 0, indice = 0;
		for(int i = 0 ; i < empregados.size(); i++) {
			if(empregados.get(i).getCod().equals(cod)) {
				indice = i;
				cont++;
			}
		}
		
		if(cont == 0) {
			String mensagem = "Empregado n�o existe no sistema!";

			String titulo = "Erro";

			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);

		}else
			if(cont == 1) {
				empregados.get(indice).setStatus("em ferias");
				String mensagem = "Status alterado com sucesso.";

				String titulo = "Sucesso";

				JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
			}else
				if(cont > 1) {
					String mensagem = "Existe mais de um empregado com este c�digo.";

					String titulo = "Erro";

					JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
				}
	}

	public void requerirAproveitamentoEmpregado() {

		// coagido no ambiente de trabalho

		String[] opcoes = { "Sim", "N�o" };

		JOptionPane.showOptionDialog(null, "J� se sentiu coagido em seu ambiente de trabalho?",
				"Formul�rio aproveitamento", 0, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[1]);

		// ass�dio

		JOptionPane.showOptionDialog(null, "J� se sentiu assediado em seu ambiente de trabalho?",
				"Formul�rio aproveitamento", 0, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[1]);

		// abuso de autoridade
		JOptionPane.showOptionDialog(null, "J� sofreu abuso de autoridade?", "Formul�rio aproveitamento", 0,
				JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[1]);

		// negligenciado
		JOptionPane.showOptionDialog(null, "J� se sentiu negligenciado em seu ambiente de trabalho?",
				"Formul�rio aproveitamento", 0, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[1]);

		// suporte
		JOptionPane.showOptionDialog(null, "J� sentiu falta de suporte em seu ambiente de trabalho?",
				"Formul�rio aproveitamento", 0, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[1]);

		// desvalorizado
		JOptionPane.showOptionDialog(null, "J� se sentiu desvalorizado em seu ambiente de trabalho?",
				"Formul�rio aproveitamento", 0, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[1]);

	}

	public void consultarInformacoesEmpregados() {

		for (int cont = 0; cont < empregados.size(); cont++) {

			empregados.get(cont).exibirEmpregado();

		}

	}

}