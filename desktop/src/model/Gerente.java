
package model;

import java.sql.Date;

public class Gerente extends Empregado {
	//contrutor da classe
	public Gerente(String email, String telefone, String endereco, String nome, String rg, String genero,
			Date dataDeNascimento, String carteiraDeTrabalho, double salario, int cargaHoraria, String funcao,
			String setor, String codGerente, String senhaGerente, String status, String foto) {
		super(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria,
				funcao, setor, senhaGerente, codGerente, status, foto);
		setCod(codGerente);
	}

	//metodo para exibir um gerente
	public void exibirGerente() {
		super.exibirEmpregado();
	}

	public void setCod(String cod) {
		super.setCod("G" + cod);
	}

}