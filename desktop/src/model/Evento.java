package model;

import java.sql.Time;
import java.util.Date;

public class Evento {
	//atributos da classe
	private String nome;
	private Date data;
	private Time hora;
	private String cod;
	private String descricao;
	private int tipo;
	
	/*
	 *	Tipos:
	 *		0: Padrao
	 *		1: Voo
	 *		2: Manutencao
	 *		3:   
 	*/
	
//	private String tipo;
//	private String local;
//	private int quantMaxPessoas;

	
	 //construtor vazio da classe
	public Evento() {
		super();
	}

	//contrutor da classe
	public Evento(String nome, Date data, Time hora, String cod, String descricao, int tipo) {
		super();
		this.nome = nome;
		this.data = data;
		this.hora = hora;
		this.cod = cod;
		this.descricao = descricao;
		this.tipo = tipo;
	}

	//getters e setters
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Time getHora() {
		return hora;
	}

	public void setHora(Time hora) {
		this.hora = hora;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	
	//metodo para exibir um evento
	public void exibirEvento() {
		System.out.println("Nome: " + getNome());
		System.out.println("Data: " + getData());
		System.out.println("Horario: " + getHora());
		System.out.println("Descricao: " + getDescricao());
	}
}
