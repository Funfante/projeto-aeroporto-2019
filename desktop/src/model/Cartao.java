package model;

public class Cartao {
	private String numero; //Atributo que ir� representar o N� do cart�o.
	private String dataDeVencimento; //Atributo que ir� representar a data de vencimento do cart�o.
	private String codigoDeSeguranca; //Atributo que ir� representar o c�digo de seguran�a do cart�o.
	private String idCartao;
	private String codPassageiro;

	//Construtor da classe cart�o:
	public Cartao(String numero, String dataDeVencimento, String codigoDeSeguranca) {
		super();
		this.numero = numero;
		this.dataDeVencimento = dataDeVencimento;
		this.codigoDeSeguranca = codigoDeSeguranca;
		this.codPassageiro = codPassageiro;
	}
	
	//Getters e Setters:
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDataDeVencimento() {
		return dataDeVencimento;
	}

	public void setDataDeVencimento(String dataDeVencimento) {
		this.dataDeVencimento = dataDeVencimento;
	}

	public String getCodigoDeSeguranca() {
		return codigoDeSeguranca;
	}

	public void setCodigoDeSeguranca(String codigoDeSeguranca) {
		this.codigoDeSeguranca = codigoDeSeguranca;
	}

	public String getCodPassageiro() {
		return codPassageiro;
	}

	public void setCodPassageiro(String codPassageiro) {
		this.codPassageiro = codPassageiro;
	}
	
}