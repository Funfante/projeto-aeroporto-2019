package model;

public class Empresa extends PessoaJuridica {
	//atributo da classe
	private int duracaoContrato;

	//construtor da classe
	public Empresa(String telefone, String email, String endereco, String cod, String senha, int cnpj,
			String nomeFantasia, int duracaoContrato, String foto) {
		super(telefone, email, endereco, cod, senha, cnpj, nomeFantasia, foto);
		this.duracaoContrato = duracaoContrato;
	}

	//get e set
	public int getDuracaoContrato() {
		return duracaoContrato;
	}

	public void setDuracaoContrato(int duracaoContrato) {
		this.duracaoContrato = duracaoContrato;
	}

	//metodo para exibir uma empresa
	public void exibirEmpresa() {
		System.out.println("Cnpj: " + getCnpj());
		System.out.println("Nome Fantasia: " + getNomeFantasia());
		System.out.println("Codigo: " + getCod());
		System.out.println("Dura��o do contrato (meses): " + getDuracaoContrato());
	}
}