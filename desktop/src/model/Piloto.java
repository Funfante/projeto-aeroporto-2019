package model;

import java.sql.Date;
public class Piloto extends Empregado{
	
	public Piloto(String email, String telefone, String endereco, String nome, String rg, String genero, Date dataDeNascimento, String carteiraDeTrabalho,
			double salario, int cargaHoraria, String funcao, String setor, String senha, String cod, String status, String foto) {
		super(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria, funcao, setor, senha, cod, status, foto);
		setCod(cod);
	}
	
	public Piloto() {
		
	}

		public void setCod(int cod) {

			super.setCod("H" + cod);

		}
}


