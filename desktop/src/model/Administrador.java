
package model;

import java.sql.Date;

// Classe para representar um Administrador:

public class Administrador extends Empregado {

	// Construtor da classe voo (cria um objeto administrador):

	public Administrador(String email, String telefone, String endereco, String nome, String rg, String genero,
			Date dataDeNascimento, String carteiraDeTrabalho, double salario, int cargaHoraria, String funcao,
			String setor, String senhaAdministrador, String cod, String status, String foto) {
		super(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria,
				funcao, setor, senhaAdministrador, cod, status, foto);
		setCod(senhaAdministrador);
	}

	// M�todo para exibir um administrador:

	public void exibirAdministrador() {
		super.exibirEmpregado();
	}

	// M�todo setSenha(), para adicionar um prefixo, nesse caso 'A', � senha do
	// Administrador:

	public void setCod(int cod) {
		super.setCod("A" + cod);
	}

}