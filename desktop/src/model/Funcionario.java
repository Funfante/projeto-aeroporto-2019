
package model;
import java.sql.Date;
public class Funcionario extends Empregado {	//construtor da classe
	public Funcionario(String email, String telefone, String endereco, String nome, String rg, String genero,			Date dataDeNascimento, String carteiraDeTrabalho, double salario, int cargaHoraria, String funcao, String setor,			 String senhaFuncionario, String cod, String status, String foto) {		super(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria,				funcao, setor, senhaFuncionario, cod, status, foto);		setCod(senhaFuncionario);	}
	
	//construtor vazio da classe
	public Funcionario() {		super();	}
	//metodo para exibir um funcionario
	public void exibirFuncionario() {		super.exibirEmpregado();	}
	public void setCod(String cod) {		super.setCod("F" + cod);	}
}