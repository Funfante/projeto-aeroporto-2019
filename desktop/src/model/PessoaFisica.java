package model;
import java.sql.Date;
public class PessoaFisica extends Pessoa {
	private String nome;
	private String rg;
	private String genero;
	private String dataDeNascimento;

	public PessoaFisica(String nome, String rg, String genero, String dataDeNascimento) {
		this.nome = nome;
		this.rg = rg;
		this.genero = genero;
		this.dataDeNascimento = dataDeNascimento;
	private String genero;
	private Date dataDeNascimento;

	public PessoaFisica(String telefone, String email, String endereco, String nome, String rg, String genero, Date dataDeNascimento, String cod, String senha, String foto) {
		super(telefone, email, endereco, cod, senha, foto);
		this.nome = nome;
		this.rg = rg;
		this.genero = genero;
		this.dataDeNascimento = dataDeNascimento;
	}


	public PessoaFisica() {
		super();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public void exibirPessoaFisica() {
		super.exibirPessoa();
		System.out.println("Nome: " + getNome());
		System.out.println("Rg: " + getRg());
		System.out.println("G�nero: " + getGenero());
		System.out.println("Data de nascimento: " + getDataDeNascimento());
		System.out.println("Co�digo:" + getCod());
	}
}

}
