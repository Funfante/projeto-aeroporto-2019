package model;

import java.util.Date;

// Classe para representar uma aeronave:
public class Aeronave {
	private String modeloAeronave; // Modelo da aeronave.
	private int capacidadeMaxima; // Capacidade maxima de passageiros suportada pela aeronave.
	private int codAeronave; // Codigo da aeronave.
	private String status; // Status da aeronave.
	private Date dataUltimaManutencao; // Data da ultima manutencao da aeronave.
	private double pesoMaximo; // Peso maximo suportado plea aeronave.
	
	// Construtor da classe Aeronave (cria um objeto aeronave):
	public Aeronave(String modeloAeronave, int capacidadeMaxima, int codAeronave, String status,
			Date dataUltimaManutencao) {
		super();
		this.modeloAeronave = modeloAeronave;
		this.capacidadeMaxima = capacidadeMaxima;
		this.codAeronave = codAeronave;
		this.status = status;
		this.dataUltimaManutencao = dataUltimaManutencao;
	}
	
	public Aeronave() {
		
	}

	// Setters e Getters:
	public String getModeloAeronave() {
		return modeloAeronave;
	}

	public void setModeloAeronave(String modeloAeronave) {
		this.modeloAeronave = modeloAeronave;
	}

	public int getCapacidadeMaxima() {
		return capacidadeMaxima;
	}

	public void setCapacidadeMaxima(int capacidadeMaxima) {
		this.capacidadeMaxima = capacidadeMaxima;
	}

	public int getCodAeronave() {
		return codAeronave;
	}

	public void setCodAeronave(int codAeronave) {
		this.codAeronave = codAeronave;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDataUltimaManutencao() {
		return dataUltimaManutencao;
	}

	public void setDataUltimaManutencao(Date dataUltimamanutencao) {
		this.dataUltimaManutencao = dataUltimamanutencao;
	}
	
	// M�todo para exibir a aeronave:
	public void exibirAeronave() {
		System.out.println("Modelo: " + getModeloAeronave());
		System.out.println("Capacidade maxima de passageiros: " + getCapacidadeMaxima());
		System.out.println("Codigo da aeronave: " + getCodAeronave());
		System.out.println("Status da aeronave: " + getStatus());
		System.out.println("Data da ultima manutencao da aeronave: " + getDataUltimaManutencao());
	}
}

