package model;
import java.sql.Date;
public class Passageiro extends PessoaFisica {
	//construtor da classe
	public Passageiro(String telefone, String email, String endereco, String nome, String rg, String genero, Date dataDeNascimento, String cod, String senha, String foto) {
		super(telefone, email, endereco, nome, rg, genero, dataDeNascimento, cod, senha, foto);
		setCod(cod);
	}

	public void setCod(String cod) {
		super.setCod("P" + cod);
	}


}
