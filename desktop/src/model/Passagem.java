package model;

//deve estar relacionada com um passagem
public class Passagem {
	private String cod;
	private String classe;
	private String codigoDeVoo;
	private double preco;

	//construtor da classe
	public Passagem(String cod, String classe, String codigoDeVoo, double preco) {
		super();
		this.cod = cod;
		this.classe = classe;
		this.codigoDeVoo = codigoDeVoo;
		this.preco = preco;
	}

	//getters e setters
	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public String getCodigoDeVoo() {
		return codigoDeVoo;
	}

	public void setCodigoDeVoo(String codigoDeVoo) {
		this.codigoDeVoo = codigoDeVoo;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

}