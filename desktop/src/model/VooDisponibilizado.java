package model;

public class VooDisponibilizado {
	private String modelo;
	private String tipoCarga;
	private int capacidadeMax;
	private int quantidadeMax;

	public VooDisponibilizado(String modelo, String tipoCarga, int capacidadeMax, int quantidadeMax) {
		super();
		this.modelo = modelo;
		this.tipoCarga = tipoCarga;
		this.capacidadeMax = capacidadeMax;
		this.quantidadeMax = quantidadeMax;
	}

	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getTipoCarga() {
		return tipoCarga;
	}
	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}
	public int getCapacidadeMax() {
		return capacidadeMax;
	}
	public void setCapacidadeMax(int capacidadeMax) {
		this.capacidadeMax = capacidadeMax;
	}
	public int getQuantidadeMax() {
		return quantidadeMax;
	}
	public void setQuantidadeMax(int quantidadeMax) {
		this.quantidadeMax = quantidadeMax;
	}


}
