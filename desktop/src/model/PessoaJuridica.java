
package model;

public class PessoaJuridica extends Pessoa {
	private int cnpj;
	private String nomeFantasia;

	public PessoaJuridica(String telefone, String email, String endereco, String cod, String senha, int cnpj, String nomeFantasia, String foto) {
		super(telefone, email, endereco, senha, cod, foto);
		this.cnpj = cnpj;
		this.nomeFantasia = nomeFantasia;
		setCod(cod);
	}

	public int getCnpj() {
		return cnpj;
	}

	public void setCnpj(int cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public void exibirPessoaJuridica() {
		super.exibirPessoa();
		System.out.println("Cnpj: " + cnpj);
		System.out.println("Nome fantasia: " + nomeFantasia);
	}
}