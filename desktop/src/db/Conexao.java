package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class Conexao {
	public static String status = "N�o conectou...";
	
	public Conexao() {
		
	}
	
	public static Connection getConexao(){
		Connection connection = null;
		try {
			String driver = "com.mysql.jdbc.Driver";//****
			Class.forName(driver);
			
			String serverNome = "localhost";
			String database = "teste";//*****
			String url = "jdbc:mysql://" + serverNome + "/" + database;
	        String username = "root"; //******     
	        String password = "ifsuldeminas"; 
	        connection = DriverManager.getConnection(url, username, password);
	  
	 
	        if (connection != null) {
	            status = "STATUS--->Conectado com sucesso!";
	        } else {
	            status = "STATUS--->N�o foi possivel realizar conex�o";
	        }
	 
	        return connection;
	 
		}catch (ClassNotFoundException e) {
			String mensagem = "O driver especificado n�o foi encontrado!";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
			return null;
		}catch(SQLException e) {
			String mensagem = "N�o foi possivel conectar ao banco de dados!";
			String titulo = "Erro";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}
	
	public static String statusConnection() {
		return status;
	}
	
	public static boolean fecharConexao() {
		try {
			Conexao.getConexao().close();
			return true;
		}catch(SQLException e) {
			return false;
		}
	}
	
	public static Connection reiniciarConexao(){
		fecharConexao();
		return Conexao.getConexao();
	}

}
