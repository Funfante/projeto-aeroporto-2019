package db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class CRUD {

	//todas tabelas
	private static final String sqlInsertPessoa = "INSERT INTO pessoa (telefone, email, endereco, nome, senha, codChar, cod) VALUES (?,?,?,?,?,?,?)";
	private static final String sqlInsertPessoaFisica = "INSERT INTO pessoa (rg, genero, dataDeNascimento, email, cod, codChar) VALUES (?,?,?,?,?,?)";
	private static final String sqlInsertJuridica = "INSERT INTO pessoa (telefone, email, endereco, nome, senha, codChar, cod) VALUES (?,?,?,?,?,?,?)";
	public boolean salvar() {
	        try{
				Connection retornoConn = Conexao.getConexao();
				PreparedStatement preparedStatement = retornoConn.prepareStatement(sqlInsertPessoa);

	            preparedStatement.setString(1, "");
	            preparedStatement.setString(2, "");
	           
	            preparedStatement.executeUpdate();
	            return true;
	        } catch (SQLException e) {
	            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
	            return true;
	        } catch (Exception e) {
	            e.printStackTrace();
	            return true;
	        }

	}
	
}
