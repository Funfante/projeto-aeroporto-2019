package view;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.*;

public class Interfaces {
	Aeroporto aeroporto = new Aeroporto();
	private ArrayList voosDisponibilizados = new ArrayList();
	private Evento chegadaVoo;
	private Evento decolagemVoo;
	private Voo vooT;
	private boolean autorizar;
	
	public void interfaceVoo(Voo voo) {
		vooT = voo;
		String opcoesVoo[] = {"Solicitar aterrissagem.", "Solicitar decolagem."};
		String opcaoVoo = (String)JOptionPane.showInputDialog(
				null,
				"O que voce deseja solicitar?",
				"Selecione",
				JOptionPane.QUESTION_MESSAGE, 
				null, 
				opcoesVoo, 
				opcoesVoo[0]);
		switch(opcaoVoo) {
			case "Solicitar aterrrissagem.":
				String mensagem = "Seu pedido foi encaminhado para o aeroporto.";
				String titulo = "Aguarde sua autorizacao.";
				JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
				chegadaVoo = voo.getChegada();
				autorizar = true;
				break;
			case "Solicitar decolagem.":
				String mensagem2 = "Seu pedido foi encaminhado para o aeroporto.";
				String titulo2 = "Aguarde sua autorizacao.";
				JOptionPane.showMessageDialog(null, mensagem2, titulo2, JOptionPane.INFORMATION_MESSAGE);
				decolagemVoo = voo.getPartida();
				autorizar = true;
				break;
		}
	}
	
	public void interfacePessoaFisica(PessoaFisica p) {
		String opcoes[] = {"Alterar nome", "Alterar email", "Alterar telefone", "Alterar endereco", "Alterar senha", "Alterar genero", "Alterar foto"};
		String opcao = (String) JOptionPane.showInputDialog(
				null,
				"O que voce deseja alterar?",
				"Selecione",
				JOptionPane.QUESTION_MESSAGE, 
				null, 
				opcoes, 
				opcoes[0]);
		
		switch(opcao) {
			case "Alterar nome":
				String nome = JOptionPane.showInputDialog("Alterar nome", "Seu novo nome...");
				p.setNome(nome);
				break;
			case "Alterar email":
				String email = JOptionPane.showInputDialog("Alterar email", "Seu novo email...");
				p.setEmail(email);
				break;
			case "Alterar telefone":
				String telefone = JOptionPane.showInputDialog("Alterar telefone", "Seu novo telefone...");
				p.setTelefone(telefone);
				break;
			case "Alterar endereco":
				String endereco = JOptionPane.showInputDialog("Alterar endereco", "Seu novo endereco...");
				p.setEndereco(endereco);
				break;
			case "Alterar senha":
				String senha = JOptionPane.showInputDialog("Alterar senha", "Seu novo senha...");
				p.setSenha(senha);
				break;
			case "Alterar genero":
				String genero = JOptionPane.showInputDialog("Alterar genero", "Seu novo genero...");
				p.setGenero(genero);
				break;
			case "Alterar foto":
				String foto = JOptionPane.showInputDialog("Alterar foto", "Endereco da sua nova foto...");
				p.setFoto(foto);
				break;
			default:
				return;
		}
	}
	
	public void interfaceEmpregado(Empregado e) {
		interfacePessoaFisica(e);
		int opcao =JOptionPane.showConfirmDialog(null, "Deseja solicitar demissao?", "Pedido de demissao",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(opcao == JOptionPane.YES_OPTION) {
			String mensagem = "Seu pedido foi encaminhado para o Rh.";
			String titulo = "Obrigado.";
			JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public void interfaceAdministrador(Empregado a) {
		interfaceEmpregado(a);
		String opcoes[] = {"Visualizar empregados.", "Visualizar empregados em ferias.", "Visualizar empregados disponiveis.", "Contratar empregado.", "Demitir empregado.", "Alterar senha empregado.", "Alterar salario empregado.", "Alterar carga horaria empregado.", "Alterar funcao empregado.", "Alterar setor empregado.", "Dar ferias para funcionario.", "Exibir agenda.", "Imprimir todos os eventos.", "Imprimir um evento especifico.", "Adicionar evento.", "Adicionar voo.", "Visualizar empresas.", "Visualizar empresa.", "Cadastrar empresa.", "Autorizar decolagem.", "Autorizar aterrissagem.", "Disponibilizar voo."};
		String opcao = (String) JOptionPane.showInputDialog(
				null,
				"O que voce deseja?",
				"Selecione",
				JOptionPane.QUESTION_MESSAGE, 
				null, 
				opcoes, 
				opcoes[0]);
		switch(opcao) {
			case "Visualizar empregados.":
				aeroporto.getRh().exibirEmpregados();
				break;
			case "Visualizar empregados em ferias.":
				aeroporto.getRh().exibirEmpregadosFerias();
				break;
			case "Visualizar empregados disponiveis.":
				aeroporto.getRh().exibirEmpregadosDisponiveis();
				break;
			case "Contratar empregado.":
				String opcoesEmpregados[] = {"Adimistrador", "Gerente", "Funcionario", "Piloto"};
				String opcaoEmpregado = (String) JOptionPane.showInputDialog(
						null,
						"Que tipo de funcionario voce deseja contratar?",
						"Selecione",
						JOptionPane.QUESTION_MESSAGE, 
						null, 
						opcoesEmpregados, 
						opcoesEmpregados[0]);
				
				
				String email = JOptionPane.showInputDialog("Contratacao", "Email...");
				String telefone = JOptionPane.showInputDialog("Contratacao", "Telefone...");
				String endereco = JOptionPane.showInputDialog("Contratacao", "Endereco...");
				String nome = JOptionPane.showInputDialog("Contratacao", "Nome...");
				String rg = JOptionPane.showInputDialog("Contratacao", "Rg...");
				String genero = JOptionPane.showInputDialog("Contratacao", "Genero...");
				Date dataDeNascimento = Date.valueOf(JOptionPane.showInputDialog("Contratacao", "Data de nascimento..."));
				String carteiraDeTrabalho =JOptionPane.showInputDialog("Contratacao", "Carteira de trabalho...");
				double salario =Double.valueOf(JOptionPane.showInputDialog("Contratacao", "Salario..."));
				int cargaHoraria =Integer.valueOf(JOptionPane.showInputDialog("Contratacao", "Carga horaria..."));
				String funcao = JOptionPane.showInputDialog("Contratacao", "Funcao...");
				String setor = JOptionPane.showInputDialog("Contratacao", "Setor...");
				String senha =JOptionPane.showInputDialog("Contratacao", "Senha...");
				String cod =JOptionPane.showInputDialog("Contratacao", "Codigo...");
				String status =JOptionPane.showInputDialog("Contratacao", "Status...");
				String foto = JOptionPane.showInputDialog("Informacao", "Foto...(endereco da imagem)");
				
				
				switch(opcaoEmpregado) {
					case "Administrador":
						Empregado administrador = new Administrador(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria, funcao, setor, senha, cod, status, foto);
						aeroporto.getAdministradores().add(administrador);
						break;
					case "Gerente":
						Empregado gerente = new Gerente(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria, funcao, setor, senha, cod, status, foto);
						aeroporto.getGerentes().add(gerente);
						break;
					case "Funcionario":
						Empregado funcionario = new Funcionario(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria, funcao, setor, senha, cod, status, foto);
						aeroporto.getFuncionarios().add(funcionario);
						break;
					case "Piloto":
						Empregado piloto = new Piloto(email, telefone, endereco, nome, rg, genero, dataDeNascimento, carteiraDeTrabalho, salario, cargaHoraria, funcao, setor, senha, cod, status, foto);
						aeroporto.getPilotos().add(piloto);
						break;
				}
				break;
				case "Demitir empregado.":
					String codD =JOptionPane.showInputDialog("Demissao", "Codigo...");
					aeroporto.getRh().demitirEmpregado(codD);
					break;
				case "Alterar senha empregado.":
					String codS =JOptionPane.showInputDialog("Informacao", "Codigo...");
					String novaSenha =JOptionPane.showInputDialog("Alteracao", "Senha...");
					aeroporto.getRh().alterarSenhaEmpregado(codS, novaSenha);
					break;
				case "Alterar salario empregado.":
					String codSa =JOptionPane.showInputDialog("Informacao", "Codigo...");
					double novoSalario =Double.valueOf(JOptionPane.showInputDialog("Altercao", "Salario..."));
					aeroporto.getRh().alterarSalarioEmpregado(codSa, novoSalario);
					break;
				case "Alterar carga horaria empregado.":
					String codC =JOptionPane.showInputDialog("Informacao", "Codigo...");
					int novaCargaHoraria =Integer.valueOf(JOptionPane.showInputDialog("Alteracao", "Carga horaria semanal..."));
					aeroporto.getRh().alterarCargaHorariaEmpregado(codC, novaCargaHoraria);
					break;
				case "Alterar funcao empregado.":
					String codF =JOptionPane.showInputDialog("Informacao", "Codigo...");
					String novaFuncao =JOptionPane.showInputDialog("Alteracao", "Funcao...");
					aeroporto.getRh().alterarFuncao(codF, novaFuncao);
					break;
				case "Alterar setor empregado.":
					String codSe =JOptionPane.showInputDialog("Informacao", "Codigo...");
					String novoSetor=JOptionPane.showInputDialog("Alteracao", "Setor...");
					aeroporto.getRh().alterarSetor(codSe, novoSetor);
					break;
				case "Dar ferias para funcionario.":
					String codFun =JOptionPane.showInputDialog("Informacao", "Codigo...");
					aeroporto.getRh().darFerias(codFun);
					break;
				case  "Exibir agenda.":
					aeroporto.getAgenda().exibirAgenda();
					break;
				case "Imprimir todos os eventos.":
					aeroporto.getAgenda().imprimirEventos();
					break;
				case "Imprimir um evento especifico.":
					String codEvento = JOptionPane.showInputDialog("Imprimir evento!", "Digite o codigo do evento que deseja visualizar: ");
					aeroporto.getAgenda().imprimirEvento(codEvento);
					break;
				case "Adicionar evento.":
					String nomeEvento = JOptionPane.showInputDialog("Adicionar o nome do evento.", "Digite o nome do evento: ");
					String codigoEvento = JOptionPane.showInputDialog("Adicionar o codigo do evento.", "Digite o codigo do evento: ");
					String descricaoEvento = JOptionPane.showInputDialog("Adicionar a descricao do evento.", "Digite a descricao do evento: ");
					String diaEvento = JOptionPane.showInputDialog("Adicionar o dia do evento.", "Digite o dia do evento: ");
					String mesEvento = JOptionPane.showInputDialog("Adicionar o mes do evento.", "Digite o mes do evento: ");
					String anoEvento = JOptionPane.showInputDialog("Adicionar o dia do evento.", "Digite o dia do evento: ");
					Date data = new Date(Integer.valueOf(diaEvento + mesEvento + anoEvento));
					Time horaEvento = Time.valueOf(JOptionPane.showInputDialog("Adicionar o horario do evento.", "Digite o horario do inicio do evento: "));
					int tipoEvento = Integer.valueOf(JOptionPane.showInputDialog("Adicionar o tipo do evento.", "Digite o tipo do evento: "));
	
					Evento evento = new Evento(nomeEvento, data, horaEvento, codigoEvento, descricaoEvento, tipoEvento);
					aeroporto.getAgenda().adicionarEvento(evento);
					break;
				case "Adicionar voo.":

					String nomeVoo = JOptionPane.showInputDialog("Adicionar o nome do voo.", "Digite o nome do voo: ");
					String codigoEventoVoo = JOptionPane.showInputDialog("Adicionar o codigo do evento.", "Digite o codigo do evento: ");
					String descricaoVoo = JOptionPane.showInputDialog("Adicionar a descricao do evento.", "Digite a descricao do evento: ");
					String diaVoo = JOptionPane.showInputDialog("Adicionar o dia do evento.", "Digite o dia do evento: ");
					String mesVoo = JOptionPane.showInputDialog("Adicionar o mes do evento.", "Digite o mes do evento: ");
					String anoVoo = JOptionPane.showInputDialog("Adicionar o dia do evento.", "Digite o dia do evento: ");
					Date dataVoo = new Date(Integer.valueOf(diaVoo + mesVoo + anoVoo));
					Time horaVoo = Time.valueOf(JOptionPane.showInputDialog("Adicionar o horario do evento.", "Digite o horario do inicio do evento: "));
					int tipoVoo = 1 ; // tipo de evento desse voo.

					Evento partida = new Evento(nomeVoo, dataVoo, horaVoo, codigoEventoVoo, descricaoVoo, tipoVoo);

					//Pensar no m�todo adicionar aeronave!

					codigoEventoVoo = JOptionPane.showInputDialog("Adicionar o codigo do evento.", "Digite o codigo do evento: ");
					diaVoo = JOptionPane.showInputDialog("Adicionar o dia previsto de chegada.", "Chegada: ");
					mesVoo = JOptionPane.showInputDialog("Adicionar o mes previsto de chegada.", "Chegada: ");
					anoVoo = JOptionPane.showInputDialog("Adicionar o ano previsto de chegada.", "Chegada: ");
					dataVoo = new Date(Integer.valueOf(diaVoo + mesVoo + anoVoo));
					horaVoo = Time.valueOf(JOptionPane.showInputDialog("Adicionar o horario do evento.", "Digite o horario do inicio do evento: "));

					Evento chegada = new Evento(nomeVoo, dataVoo, horaVoo, codigoEventoVoo, descricaoVoo, tipoVoo);

					ArrayList<String> aeronavesDisponiveis = new ArrayList();
					
					for(int i = 0; i < aeroporto.getAeronaves().size(); i++){
						if(aeroporto.getAeronaves().get(i).getStatus().equals("disponivel")) {
							aeronavesDisponiveis.add(aeroporto.getAeronaves().get(i).getModeloAeronave());
						}
					}
				
					opcoes = aeronavesDisponiveis.toArray(opcoes);

					int codAeronave = Integer.valueOf((String)JOptionPane.showInputDialog(null, "Voce tem acesso as seguintes aeronaves:", "Selecione a aeronave desejada:", JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]));

					opcoes = new String[2];
					opcoes[0] = "Carga";
					opcoes[1] = "Pessoas";

					String opcaoCP =  (String) JOptionPane.showInputDialog(null, "Transporte:", "Selecione o tipo de transporte que sera feito:", JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]);
					String tipoCarga = JOptionPane.showInputDialog("Qual sera a carga transportada?", "Carga:");
					String codigoDeVoo = JOptionPane.showInputDialog("Codigo do voo.", "Codigo do voo:");
					String senhaVoo = JOptionPane.showInputDialog("Codigo do voo.", "Senha do voo:");
					String destino =  JOptionPane.showInputDialog("Destino de voo.", "Destino do voo:");

					String origem = JOptionPane.showInputDialog("Origem de voo.", "Origem de voo:");

					String statusVoo = JOptionPane.showInputDialog("Status de voo.", "Status de voo:");

					ArrayList<String> pilotosDisponiveis = new ArrayList();

					for (int i=0; i<aeroporto.getPilotos().size();i++) {
						if(aeroporto.getPilotos().get(i).getStatus().equals("disponivel")) {
							pilotosDisponiveis.add(aeroporto.getPilotos().get(i).getCod());
						}
					}

					String opcaoPiloto[]= {};
					opcaoPiloto = pilotosDisponiveis.toArray(opcaoPiloto);

					String codPiloto = String.valueOf(JOptionPane.showInputDialog(null, "Pilotos disponiveis.", "Selecione o piloto disponivel desejado:", JOptionPane.QUESTION_MESSAGE, null, opcaoPiloto, opcaoPiloto[0]));

					int qtdPassageiros = Integer.valueOf(JOptionPane.showInputDialog("Quantidade de Passageiros.", "Quantidade de passageiros no voo:"));

					boolean valido = false;

					Aeronave aeronave = new Aeronave();
					for(int i = 0 ; i < aeroporto.getAeronaves().size(); i++) {
						if (aeroporto.getAeronaves().get(i).getCodAeronave() == codAeronave) {
							while(!valido) {
								if (qtdPassageiros<=aeroporto.getAeronaves().get(i).getCapacidadeMaxima()) {
									valido = true;
								} else {
									qtdPassageiros = Integer.valueOf(JOptionPane.showInputDialog("Quantidade de Passageiros.", "Quantidade de passageiros no voo:"));
								}
							}
							aeronave = aeroporto.getAeronaves().get(i);
						}
					}

					Voo voo = new Voo(partida, chegada, "nao", tipoCarga, opcaoCP, codigoDeVoo, senhaVoo, statusVoo, descricaoVoo, aeronave, destino, origem, codPiloto, qtdPassageiros);

					aeroporto.getVoos().add(voo);

					break;
					
				case "Visualizar empresas.":
					for(int i = 0; i < aeroporto.getEmpresas().size(); i++) {
						aeroporto.getEmpresas().get(i).exibirEmpresa();
					}
					break;
				case "Visualizar empresa.":
					String codEmpresaConsulta =JOptionPane.showInputDialog("Informacao", "Codigo...");
					for(int i = 0; i < aeroporto.getEmpresas().size(); i++) {
						if(aeroporto.getEmpresas().get(i).getCod().equals(codEmpresaConsulta)) {
							aeroporto.getEmpresas().get(i).exibirEmpresa();
						}
						
					}
					break;
				case "Cadastrar empresa.":
					String telefoneEmpresa =JOptionPane.showInputDialog("Informacao", "Telefone...");
					String emailEmpresa =JOptionPane.showInputDialog("Informacao", "Email...");
					String enderecoEmpresa = JOptionPane.showInputDialog("Informacao", "Endereco...");
					String codEmpresa = JOptionPane.showInputDialog("Informacao", "Codigo...");
					String senhaEmpresa = JOptionPane.showInputDialog("Informacao", "Senha...");
					int cnpjEmpresa = Integer.valueOf(JOptionPane.showInputDialog("Informacao", "Cnpj..."));
					String nomeEmpresa = JOptionPane.showInputDialog("Informacao", "Nome...");
					int duracaoEmpresa = Integer.valueOf(JOptionPane.showInputDialog("Informacao", "Duracao contrato..."));
					String fotoEmpresa = JOptionPane.showInputDialog("Informacao", "Foto...(endereco da imagem)");
					Empresa empresa = new Empresa(telefoneEmpresa, emailEmpresa, enderecoEmpresa, codEmpresa, senhaEmpresa, cnpjEmpresa, nomeEmpresa, duracaoEmpresa, fotoEmpresa);
					aeroporto.getEmpresas().add(empresa);
					break;
				case "Autorizar decolagem.":
					if(autorizar) {
						boolean autorizar = false;
						for(int i = 0; i < aeroporto.getVoos().size(); i++) {
							if(aeroporto.getVoos().get(i).getPartida().equals(decolagemVoo)) {
								autorizar = false;
							}
						}
						if(!autorizar) {
							String mensagem = "Impossivel a decolagem";
							String titulo = "Erro.";
							JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
						}else
							if(autorizar) {
								int opcaoAutorizacao = JOptionPane.showConfirmDialog(null, "Autorizar decolagem?", "Autorizacao",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
								if(opcaoAutorizacao == JOptionPane.YES_OPTION) {
									vooT.setAutorizado("sim");
								}
							}
					}else
						if(!autorizar) {
							String mensagem = "Nao ha nenhuma autorizacao pendente";
							String titulo = "Erro.";
							JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
						}
					break;
				case "Autorizar aterrissagem":
					if(autorizar) {
						boolean autorizarA = false;
						for(int i = 0; i < aeroporto.getVoos().size(); i++) {
							if(aeroporto.getVoos().get(i).getChegada().equals(chegadaVoo)) {
								autorizarA = false;
							}
						}
						if(!autorizarA) {
							String mensagem = "Impossivel a aterrissagem";
							String titulo = "Erro.";
							JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
						}else
							if(autorizarA) {
								int opcaoAutorizacao = JOptionPane.showConfirmDialog(null, "Autorizar aterrissagem?", "Autorizacao",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
								if(opcaoAutorizacao == JOptionPane.YES_OPTION) {
									vooT.setAutorizado("sim");
								}
							}
					}else
						if(!autorizar) {
							String mensagem = "Nao ha nenhuma autorizacao pendente";
							String titulo = "Erro.";
							JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
						}
					break;
				case "Disponibilizar voo.":
					String modeloAeronave = JOptionPane.showInputDialog("Modelo de voo.", "Modelo de voo...");
					String opcoesVoo[] = {"Carga", "Pessoas"};
					String opcaoCPVoo =  (String) JOptionPane.showInputDialog(null, "Transporte.", "Selecione o tipo de transporte que sera feito...", JOptionPane.QUESTION_MESSAGE, null, opcoesVoo, opcoesVoo[0]);
					if(opcaoCPVoo.equals("Carga")) {
						int capacidadeVoo = Integer.valueOf(JOptionPane.showInputDialog("Capacidade maxima de carga no voo.", "Capacidade..."));
						VooDisponibilizado vooC = new VooDisponibilizado(modeloAeronave, opcaoCPVoo, capacidadeVoo, 0);
						voosDisponibilizados.add(vooC);
					}else
						if(opcaoCPVoo.equals("Pessoas")) {
							int qtPVoo = Integer.valueOf(JOptionPane.showInputDialog("Quantidade maxima de pessoas no voo.", "Quantidade..."));
							VooDisponibilizado vooP = new VooDisponibilizado(modeloAeronave, opcaoCPVoo, 0, qtPVoo);
							voosDisponibilizados.add(vooP);
						}
				break;
			default:
					return;
		}
	}
	
	public void interfaceGerente(Empregado g) {
		interfaceEmpregado(g);
		String opcoes[] = {"Visualizar empregados.", "Visualizar empregados em ferias.", "Visualizar empregados disponiveis.", "Alterar funcao empregado.", "Alterar setor empregado.", "Exibir agenda.", "Imprimir todos os eventos.", "Imprimir um evento especifico."};
		String opcao = (String) JOptionPane.showInputDialog(null, "O que voce deseja?", "Selecione", JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]);
		switch(opcao) {
			case "Visualizar empregados.":
				aeroporto.getRh().exibirEmpregados();
				break;
			case "Visualizar empregados em ferias.":
				aeroporto.getRh().exibirEmpregadosFerias();
				break;
			case "Visualizar empregados disponiveis.":
				aeroporto.getRh().exibirEmpregadosDisponiveis();
				break;
			case "Alterar funcao empregado.":
				String codF =JOptionPane.showInputDialog("Informacao", "Codigo...");
				String novaFuncao =JOptionPane.showInputDialog("Alteracao", "Funcao...");
				aeroporto.getRh().alterarFuncao(codF, novaFuncao);
				break;
			case "Alterar setor empregado.":
				String codSe =JOptionPane.showInputDialog("Informacao", "Codigo...");
				String novoSetor=JOptionPane.showInputDialog("Alteracao", "Setor...");
				aeroporto.getRh().alterarSetor(codSe, novoSetor);
				break;
				case "Exibir agenda.":
				aeroporto.getAgenda().exibirAgenda();
				break;
			case "Imprimir todos os eventos.":
				aeroporto.getAgenda().imprimirEventos();
				break;
			case "Imprimir um evento especifico.":
				String codEvento = JOptionPane.showInputDialog("Imprimir evento!", "Digite o codigo do evento que deseja visualizar: ");
				aeroporto.getAgenda().imprimirEvento(codEvento);
				break;
			default:
				return;
		}
	}
	
	
	public void interfaceFuncionario(Empregado f) {
		interfaceEmpregado(f);
	}
	
}
	